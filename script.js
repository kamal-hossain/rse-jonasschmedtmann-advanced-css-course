let showOrHide = document.querySelectorAll('.showorhide'); // Selecting all out side button
let container = document.querySelectorAll('.container'); //  Selecting all inner container

// Hinding All inner container
for (let i = 0; i < showOrHide.length; i++) {
  container[i].style.display = 'none';
}

// HIDE or SHOW Div from outer button
for (let i = 0; i < showOrHide.length; i++) {
  showOrHide[i].addEventListener('click', () => {
    showOrHide[
      i
    ].parentElement.parentElement.parentElement.lastElementChild.style.display =
      showOrHide[i].parentElement.parentElement.parentElement.lastElementChild
        .style.display == 'none'
        ? 'flex'
        : 'none';

    if (
      showOrHide[i].parentElement.parentElement.parentElement.lastElementChild
        .style.display == 'none'
    ) {
      showOrHide[i].innerHTML = 'Show &darr;';
    } else showOrHide[i].innerHTML = 'Hide &uarr;';
  });
}

// Hide or Show from Inner button
let showOrHideInside = document.querySelectorAll('.showorhide-inside');

for (let i = 0; i < showOrHide.length; i++) {
  showOrHideInside[i].addEventListener('click', () => {
    showOrHideInside[
      i
    ].parentElement.parentElement.parentElement.style.display =
      showOrHideInside[i].parentElement.parentElement.parentElement.style
        .display == 'none'
        ? 'flex'
        : 'none';
    // console.log(showOrHideInside[i].parentElement.parentElement.parentElement);
    if (
      showOrHideInside[i].parentElement.parentElement.parentElement.style
        .display == 'none'
    ) {
      showOrHideInside[
        i
      ].parentElement.parentElement.parentElement.style.display = 'Hide &darr;';
    } else showOrHideInside[i].innerHTML = 'Show &uarr;';
  });
}
